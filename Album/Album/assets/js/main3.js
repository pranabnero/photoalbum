﻿jQuery(document).ready(function ($) {
	//trigger the animation - open modal window
	$('[data-type="modal-trigger3"]').on('click', function () {
		var actionBtn = $(this),
			scaleValue = retrieveScale(actionBtn.next('.cd-modal-bg3'));

		actionBtn.addClass('to-circle3');
		actionBtn.next('.cd-modal-bg3').addClass('is-visible3').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
			animateLayer(actionBtn.next('.cd-modal-bg3'), scaleValue, true);
		});

		//if browser doesn't support transitions...
		if (actionBtn.parents('.no-csstransitions').length > 0) animateLayer(actionBtn.next('.cd-modal-bg3'), scaleValue, true);
	});

	//trigger the animation - close modal window
	$('.cd-section3 .cd-modal-close3').on('click', function () {
		closeModal();
	});
	$(document).keyup(function (event) {
		if (event.which == '27') closeModal();
	});

	$(window).on('resize', function () {
		//on window resize - update cover layer dimention and position
		if ($('.cd-section3.modal-is-visible3').length > 0) window.requestAnimationFrame(updateLayer);
	});

	function retrieveScale(btn) {
		var btnRadius = btn.width() / 2,
			left = btn.offset().left + btnRadius,
			top = btn.offset().top + btnRadius - $(window).scrollTop(),
			scale = scaleValue(top, left, btnRadius, $(window).height(), $(window).width());

		btn.css('position', 'fixed').velocity({
			top: top - btnRadius,
			left: left - btnRadius,
			translateX: 0,
		}, 0);

		return scale;
	}

	function scaleValue(topValue, leftValue, radiusValue, windowW, windowH) {
		var maxDistHor = (leftValue > windowW / 2) ? leftValue : (windowW - leftValue),
			maxDistVert = (topValue > windowH / 2) ? topValue : (windowH - topValue);
		return Math.ceil(Math.sqrt(Math.pow(maxDistHor, 2) + Math.pow(maxDistVert, 2)) / radiusValue);
	}

	function animateLayer(layer, scaleVal, bool) {
		layer.velocity({ scale: scaleVal }, 400, function () {
			$('body').toggleClass('overflow-hidden', bool);
			(bool)
				? layer.parents('.cd-section3').addClass('modal-is-visible3').end().off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend')
				: layer.removeClass('is-visible3').removeAttr('style').siblings('[data-type="modal-trigger3"]').removeClass('to-circle3');
		});
	}

	function updateLayer() {
		var layer = $('.cd-section3.modal-is-visible3').find('.cd-modal-bg3'),
			layerRadius = layer.width() / 2,
			layerTop = layer.siblings('.btn').offset().top + layerRadius - $(window).scrollTop(),
			layerLeft = layer.siblings('.btn').offset().left + layerRadius,
			scale = scaleValue(layerTop, layerLeft, layerRadius, $(window).height(), $(window).width());

		layer.velocity({
			top: layerTop - layerRadius,
			left: layerLeft - layerRadius,
			scale: scale,
		}, 0);
	}

	function closeModal() {
		var section = $('.cd-section3.modal-is-visible3');
		section.removeClass('modal-is-visible3').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
			animateLayer(section.find('.cd-modal-bg3'), 1, false);
		});
		//if browser doesn't support transitions...
		if (section.parents('.no-csstransitions').length > 0) animateLayer(section.find('.cd-modal-bg3'), 1, false);
	}
});